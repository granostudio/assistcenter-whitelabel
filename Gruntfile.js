module.exports = function(grunt) {
  "use strict";
  grunt.initConfig({
    //server local -------------------------------------------------------------
    php: {
        local: {
            options: {
                port: 9001,
                hostname: 'localhost',
                base: 'wordpress',
                keepalive: false,
                open: true,
                silent: true,
            },
        },
        watch: {
          options: {
          port: 9001,
          hostname: 'localhost',
          base: 'wordpress',
          open: true,
          options: {
              livereload: {
                host: 'localhost',
                port: 9001,
              }
            },
          }
        }
    },
    //copy frontend para wp ----------------------------------------------------
    copy: {
      dev: {
        files: [
          {expand: true, cwd: 'frontend/', src: ['**'], dest: 'wordpress/wp-content/themes/granostudio-child/'}
        ],
      },
      update_copy: {
        files: [
          {expand: true, cwd: 'granoexpresso/root/wordpress', src: ['**'], dest: 'wordpress/'}
        ],
      }
    },
    // COMPASS ----------------------------------------------------------------
    compass: { // Task
      din: { // tema mãe
        options: {  // Target options
          sassDir: 'dinamico/sass',
          cssDir: 'dinamico/css',
          environment: 'production',
          outputStyle: 'expanded',
          // require: 'bootstrap-sass'
        }
      },
      inst: { // tema mãe
        options: {  // Target options
          sassDir: 'institucional/sass',
          cssDir: 'institucional/css',
          environment: 'production',
          outputStyle: 'expanded',
          // require: 'bootstrap-sass'
        }
      }
    },
    // CSSMIN ----------------------------------------------------------------
    cssmin: {
        options: {
          shorthandCompacting: false,
          roundingPrecision: -1
        },
        din: {
          files: {
            'dinamico/css/cadastro.min.css': 'dinamico/css/cadastro.css',
            'dinamico/css/areadocliente.min.css': 'dinamico/css/areadocliente.css',
            'dinamico/css/ui.min.css': 'dinamico/css/ui.css'
          }
        },
        inst: {
          files: {
            'institucional/css/index.min.css': 'institucional/css/index.css',
            'institucional/css/ui.min.css': 'institucional/css/ui.css'
          }
        }
    },
    // WATCH ----------------------------------------------------------------
    watch: {
      din: {
        files: ['dinamico/sass/**', 'dinamico/js/cadastro-script.js', 'dinamico/**.html', 'dinamico/js/areadocliente-script.js'],
        tasks: [
          'compass:din',
          'cssmin:din',
          'uglify:din'
          ],
        options: {
          livereload: true,
        },
      },
      inst: {
        files: ['institucional/sass/**', 'institucional/js/index.js', 'institucional/**.html'],
        tasks: [
          'compass:inst',
          'cssmin:inst',
          'uglify:inst'
          ],
        options: {
          livereload: true,
        },
      }
    },
    // UGLIFY ----------------------------------------------------------------
    uglify: {
      din: {
        files: {
          'dinamico/js/cadastro-script.min.js': 'dinamico/js/cadastro-script.js',
          'dinamico/js/areadocliente-script.min.js': 'dinamico/js/areadocliente-script.js'
        }
      },
      dev: {
        files: {
          'frontend/js/dist/scripts.min.js': [ 'frontend/js/src/jquery-3.1.1.js', 'frontend/js/src/plugins/*.js', 'frontend/js/src/scripts.js'],
          'frontend/js/dist/grano-pagetemplate.min.js': 'frontend/js/src/grano-pagetemplate.js',
          'frontend/js/dist/customize-preview.min.js': 'frontend/js/src/customize-preview.js'
        }
      },
      inst: {
        files: {
          'institucional/js/index.min.js': 'institucional/js/index.js',
        }
      },
    },
    // EXEC -------------------------------------------------------------------
    exec: {
      update_rm: {
        cmd: 'git rm -f granoexpresso',
      },
      update_submodule: {
        cmd: 'git submodule add --force https://bitbucket.org/granostudio/granoexpresso.git; git submodule foreach git pull origin master',
      }
    }
  });

  grunt.loadNpmTasks('grunt-php');
  grunt.loadNpmTasks('grunt-open');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-exec');
  grunt.loadNpmTasks('grunt-ftpush');




  // --------------------------------------------------------------------------------------
  // TASKs
  // --------------------------------------------------------------------------------------
  // instalação inicial do wp
  grunt.registerTask('grano-wp', ['ftpush:live','ftpush:dev','open:dev','open:live']);
  // desenvolvimento dev
  grunt.registerTask('grano-inst', ['watch:inst']);
  // desenvolvimento local
  grunt.registerTask('grano-din', ['watch:din']);

  // colocar o site no ar
  grunt.registerTask('grano-live', ['copy:dev','ftpush:live','watch:live']);

  // des. local do tema original
  grunt.registerTask('admin-local', ['php:local','watch:admin']);

  // update grano expresso
  grunt.registerTask('grano-update', ['exec:update_submodule','copy:update_copy', 'exec:update_rm', 'copy:dev']);

  // --------------------------------------------------------------------------------------

};
