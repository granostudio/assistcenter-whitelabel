// formatar inputs
function formatar(mascara, documento){
  var i = documento.value.length;
  var saida = mascara.substring(0,1);
  var texto = mascara.substring(i)

  if (texto.substring(0,1) != saida){
            documento.value += texto.substring(0,1);
  }

}

jQuery(document).ready(function($) {
  var acc = $(".accordion"),
      btnCartao = $('.btnCartao'),
      btnCondicoes = $('.btnCondicoes'),
      btnAlterarCartao = $('.btnAlterarCartao'),
      modalCartao = $('.modal-cartao'),
      btnEditar = $('.btnEditar'),
      meusDados = $('.meus-dados'),
      btnCancelar = $('.btnCancelar'),
      inputNome = $('.nome'),
      inputEmail = $('.email'),
      inputCPF = $('.cpf'),
      inputTel = $('.telefone');

  // meus dados
  function alterarInputs(){
    if(inputNome.val()){
      inputNome.attr('placeholder', inputNome.val());
      inputNome.val('');
    }
    if(inputEmail.val()){
      inputEmail.attr('placeholder', inputEmail.val());
      inputEmail.val('');
    }
    if(inputCPF.val()){
      inputCPF.attr('placeholder', inputCPF.val());
      inputCPF.val('');
    }
    if(inputTel.val()){
      inputTel.attr('placeholder', inputTel.val());
      inputTel.val('');
    }
  }

  meusDados.find('.form-control').on('click', function(event) {
    if(btnEditar.text()=='Editar'){
      btnEditar.text('Salvar')
      meusDados.find('.form-group').addClass('active');
      btnEditar.toggleClass('btn-primary').toggleClass('btn-warning');
      meusDados.find('.btnMDCancelar').addClass('active');
    }
    alterarInputs();
  });

  btnEditar.on('click',function(event) {
    meusDados.find('.form-group').toggleClass('active');
    $(this).toggleClass('btn-primary').toggleClass('btn-warning');
    if($(this).text()=='Editar'){
      $(this).text('Salvar')
        meusDados.find('.btnMDCancelar').addClass('active');
    }else{
      $(this).text('Editar')
        meusDados.find('.btnMDCancelar');
        meusDados.find('.btnMDCancelar').removeClass('active');
    }
    alterarInputs();
  });

  // esc
  $(document).keyup(function(e) {
     if (e.keyCode == 27) { // escape key maps to keycode `27`
        meusDados.find('.form-group').removeClass('active');
        btnEditar.removeClass('btn-primary').addClass('btn-warning');
        btnEditar.text('Editar');
        inputNome.val('');
        inputEmail.val('');
        inputCPF.val('');
        inputTel.val('');
    }
  });
  meusDados.find('.btnMDCancelar').on('click', function() {
    meusDados.find('.form-group').removeClass('active');
    btnEditar.removeClass('btn-primary').addClass('btn-warning');
    btnEditar.text('Editar');
    inputNome.val('');
    inputEmail.val('');
    inputCPF.val('');
    inputTel.val('');
    $(this).removeClass('active');
  });


  // abrirModal
  function abrirModal(modal){
    $(modal).addClass('ativo');
    $(modal).find('.btnFechar').on('click', function() {
      $(modal).removeClass('ativo');
    });
  }

  // lista
      acc.click(function(){
        $(this).toggleClass('active');
    		$(this).parent().find('.content').slideToggle("slow");
    	});


      // condições gerais
      btnCondicoes.click(function(event) {
        $(this).toggleClass('active');
    		$(this).parent().find('.contentCondicoes').slideToggle("slow");
      });

      // dados do cartão
      btnCartao.on('click', function(){
        abrirModal('.modal-cartao');
        modalCartao.removeClass('active');
      });

      btnAlterarCartao.on('click', function(event) {
        modalCartao.addClass('active');
      });

      // cancelar
      btnCancelar.on('click', function(){
          abrirModal('.modal-cancelar');
      });
});
